# Nginx from DotDeb.org
#
# Version   0.0.2

FROM        mariorez/debian-php56
MAINTAINER  Mario Rezende <mariorez@gmail.com>

# Needed for edit files inside container and
# to suppress messages like "debconf: unable to initialize frontend: Dialog"
ENV TERM linux

RUN apt-get update \
    && apt-get install -y nginx

# Create user "docker" with same host user UID to avoid permissions issues for "data volume"
RUN useradd -u 1000 docker

WORKDIR /etc/nginx

# Change Nginx defualt user (www-data) to docker
RUN sed -i "s/www-data/docker/" nginx.conf \
    && echo "daemon off;" >> nginx.conf \
    && rm sites-enabled/default

COPY ./domain.conf /etc/nginx/sites-available/

RUN ln -nfs /etc/nginx/sites-available/domain.conf /etc/nginx/sites-enabled/domain.conf

EXPOSE 80

ENTRYPOINT ["nginx"]
