server {
    server_name domain-placeholder.dev;
    root /var/www/domain-placeholder.dev/web;

    location / {
        # try to serve file directly, fallback to rewrite
        try_files $uri @rewriteapp;
    }

    location @rewriteapp {
        # rewrite all to app.php
        rewrite ^(.*)$ /app.php/$1 last;
    }

    location ~ ^/(app|app_dev|config)\.php(/|$) {
        # php = env variable for link to PHP Docker Container
        fastcgi_pass php:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
    }

    location ~* \.(jpg|jpeg|gif|png|css|js|ico)$ {
        access_log        off;
        log_not_found     off;
        expires           30d;
    }

    location ~ /\. {
        access_log off;
        log_not_found off;
        deny all;
    }

    fastcgi_buffers            4 256k;
    fastcgi_buffer_size        128k;
    fastcgi_busy_buffers_size  256k;

    error_log /var/log/nginx/domain-placeholder.dev_error.log;
    access_log /var/log/nginx/domain-placeholder.dev_access.log;
}
